RSS Reader  
Project for reading articles from RSS
- Linux used, Python 3.10
- 'python3 -m venv .venv' - create environment
- 'source venv/bin/activate' - activate environment
- 'docker-compose up -d' - installing PostgreSQL
- 'pip install -r requirements.txt' - installing packages
- 'python manage.py migrate' - migrate changes
- 'python manage.py runserver' - run server on localhost:8000