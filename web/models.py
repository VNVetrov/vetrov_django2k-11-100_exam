from django.db import models


class RSS(models.Model):
    title = models.CharField(max_length=255)
    link = models.URLField(unique=True)


class Article(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    link = models.URLField()
    rss = models.ForeignKey(RSS, on_delete=models.CASCADE)
