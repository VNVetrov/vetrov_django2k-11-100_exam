import feedparser


def get_rss_title(link):
    rss = feedparser.parse(link)
    title = rss.feed.title
    return title
