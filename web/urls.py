from django.urls import path

from web.views import status_view, main_view

urlpatterns = [
    path('status/', status_view, name='status'),
    path('', main_view, name='main')
]
