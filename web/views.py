from django.http import HttpResponse
from django.shortcuts import render

from web.forms import RSSForm
from web.models import RSS
from web.utils import get_rss_title


def status_view(request):
    return HttpResponse("OK")


def main_view(request):
    status = ""
    form = RSSForm()
    rsses = RSS.objects.all()
    if request.method == "POST":
        form = RSSForm(request.POST)
        if form.is_valid():
            title = get_rss_title(form.data['link'])
            #rss = RSS.objects.create(title=title, link=form.data['link'])
            status = 'Success!'
    return render(request, template_name='web/main_page.html', context={'form': form,
                                                                        'status': status,
                                                                        'rsses': rsses})


