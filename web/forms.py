from django import forms

from web.models import RSS


class RSSForm(forms.ModelForm):
    class Meta:
        model = RSS
        fields = ('link',)
